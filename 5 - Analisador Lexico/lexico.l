%{

#include <stdio.h>
//#include "sintatico.h"
int numLinha = 1;

%}

identificador	[a-zA-Z]([a-zA-Z0-9])*
numero		[0-9]+
espaco		[ \t]+
novalinha	[\n]

%x comentario

%%

programa	{printf("Palavra Reservada: %11s\n", yytext);}
inicio		{printf("Palavra Reservada: %11s\n", yytext);}
fimprograma	{printf("Palavra Reservada: %11s\n", yytext);}

leia		{printf("Palavra Reservada: %11s\n", yytext);}
escreva		{printf("Palavra Reservada: %11s\n", yytext);}

se		{printf("Palavra Reservada: %11s\n", yytext);}
entao		{printf("Palavra Reservada: %11s\n", yytext);}
senao		{printf("Palavra Reservada: %11s\n", yytext);}
fimse		{printf("Palavra Reservada: %11s\n", yytext);}

enquanto	{printf("Palavra Reservada: %11s\n", yytext);}
faca		{printf("Palavra Reservada: %11s\n", yytext);}
fimenquanto	{printf("Palavra Reservada: %11s\n", yytext);}

inteiro		{printf("Palavra Reservada: %11s\n", yytext);}
logico		{printf("Palavra Reservada: %11s\n", yytext);}
V		{printf("Constante Lógica: %11s\n", yytext);}
F		{printf("Constante Lógica: %11s\n", yytext);}

e		{printf("Palavra Reservada: %11s\n", yytext);}
ou		{printf("Palavra Reservada: %11s\n", yytext);}
nao		{printf("Palavra Reservada: %11s\n", yytext);}

"="		{printf("Operador de Atribuição: %11s\n", yytext);}
"("		{printf("Abre Parênteses: %11s\n", yytext);}
")"		{printf("Fecha Parênteses: %11s\n", yytext);}

">"		{printf("Operador Aritmético: %11s\n", yytext);}
"<"		{printf("Operador Aritmético: %11s\n", yytext);}
"=="		{printf("Operador Aritmético: %11s\n", yytext);}
"*"		{printf("Operador Aritmético: %11s\n", yytext);}
"+"		{printf("Operador Aritmético: %11s\n", yytext);}
"-"		{printf("Operador Aritmético: %11s\n", yytext);}
"/"		{printf("Operador Aritmético: %11s\n", yytext);}
"^"		{printf("Operador Aritmético: %11s\n", yytext);}

[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]       { /* DO NOTHING */ }
[/][*]                                    { printf("Unterminated comment"); }
"//".*		/* COMENTARIO DE LINHA */

{identificador}	{ /*strcpy (atomo, yytext);*/
		printf("Identificador: %11s\n", yytext);}
{numero}	{ /*strcpy (atomo, yytext);*/	
		printf("Número: %11s\n", yytext);}
{espaco}	{ printf(" ");}

{novalinha}	{ printf("\n"); numLinha++;}

.		printf("Erro Lexico: %11s\n", yytext);

%%

int main(int argc, char **argv)
{
   yylex();
   return 0;
}
