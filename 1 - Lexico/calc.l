%{

int token = 1;
int linha = 1;

%}

%%

"+"    		{ printf("MAIS "); token++; }
"-"    		{ printf("MENOS "); token++; }
"*"    		{ printf("VEZES "); token++; }
"/"    		{ printf("DIVIDIR "); token++; }
"|"    		{ printf("ABSOLUTO "); token++; }
[0-9]+ 		{ printf("NUMERO %s ", yytext); token++; }
\n		{ printf("\nNOVA LINHA\n"); linha++; }
[ \t]		{ token++; }
[a-zA-Z]+	{ printf("Erro: %s, no token: %d, na linha: %d\n", yytext, token, linha); }

%%

int main(int argc, char **argv)
{
   yylex();
   return 0;
}
