/***********************************************
* Função responsável por gerar código em C
* Paulo Muniz de Ávila. 
************************************************/


enum instrucao {cabecalho=1,declarar_inteiro=2,declarar_boolean=3,fechar_contexto=4,leitura=5,imprime=6,se=7,
senao=8,fimsenao=9,fimse=10};

void emitTab() {
   int i;
   for(i = 0; i < identa; i++)
      printf("   ");
}

void emit(int c,...) {
    char *id, *aux;
    int tipo;
     va_list argp;

   switch(c) {
	case cabecalho:
		printf("#include <stdio.h>\n");
		printf("#include <stdlib.h>\n");
		printf("#include <math.h>\n");
		printf("#include <stdbool.h>\n");
		printf("\nint main() {\n");
		break;

        case declarar_inteiro:
		emitTab();
		printf("int ");
   	        va_start (argp, c);
  	        id = va_arg(argp, char *); 
	        printf("%s", id);
                printf(";\n");
                va_end(argp);
		break;

        case declarar_boolean:
		emitTab();
		printf("bool ");
                va_start (argp, c);
  	        id = va_arg(argp, char *); 
	        printf("%s", id);
                printf(";\n");
                va_end(argp);
		break;

	  case fechar_contexto:
		printf("\nreturn 0; \n}\n");
 		break;

          case leitura:
		emitTab();
		printf("scanf(");
   	        va_start (argp, c);
  	        id = va_arg(argp, char *); 
                printf("\"%cd\",&%s", '%',id);
                printf(");\n");
                va_end(argp);
		break;

 	  case imprime:
		emitTab();
		printf("printf(");
   	        va_start (argp, c);
  	        id = va_arg(argp, char *); 
                tipo = va_arg(argp, int);
                if(tipo == 1) {
                  printf("%s ? \"true\" : \"false\"", id);
                } else {
                   printf("\"%cd\",%s", '%',id);
                }
                printf(");\n");
                va_end(argp);
		break;

	  case se: 
		emitTab();
		printf("if(");
		va_start (argp, c);
		id = va_arg(argp, char*);
		printf("%s", id);
		printf(") {\n");
		va_end(argp);
		break;

	  case senao:
		emitTab();
		printf("} else {\n");
		break;

	  case fimse:
		emitTab();
		printf("}\n");
		break;
   }
}


