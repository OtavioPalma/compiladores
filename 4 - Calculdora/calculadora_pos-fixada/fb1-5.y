/* simplest version of calculator */

%{
   #include <stdio.h>
   #include <stdlib.h>
   #include <math.h>
   #define YYDEBUG 1 
   extern int yylex();
   extern int yyparse();
   extern FILE* yyin;
   void yyerror(const char* s);
%}



/* declare tokens */
%token NUMBER
%token ADD SUB MUL DIV EXPO
%token EOL

%%

calclist: /* nothing */
	| calclist exp EOL { printf("= %d\n", $2); } 
;

exp: term 
	| exp exp ADD { $$ = $1 + $2; }
	| exp exp SUB { $$ = $1 - $2; }
	| exp exp MUL { $$ = $1 * $2; }
	| exp exp DIV { $$ = $1 / $2; }
	| exp exp EXPO { $$ = pow($1, $2); }
;

term: NUMBER  { $$ = $1 ;}
;

%%
int main(int argc, char **argv)
{
        # if YYDEBUG == 1
        extern int yydebug;
        yydebug = 1;
        #endif
	yyparse();
        return 0;
}

void yyerror(const char* s) {
	fprintf(stderr, "Parse error: %s\n", s);
	exit(1);
}

