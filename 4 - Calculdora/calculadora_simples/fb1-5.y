/* simplest version of calculator */

%{
   #include <stdio.h>
   #include <stdlib.h>
   #define YYDEBUG 1 
   extern int yylex();
   extern int yyparse();
   extern FILE* yyin;
   void yyerror(const char* s);
%}



/* declare tokens */
%token NUMBER
%token ADD SUB MUL DIV 
%token EOL

%left ADD SUB
%left MUL DIV

%%

calclist: /* nothing */
	| calclist exp EOL { printf("= %d\n", $2); } 
;

exp: term 
	| exp ADD exp { $$ = $1 + $3; }
	| exp SUB exp { $$ = $1 - $3; }
	| exp MUL exp { $$ = $1 * $3; }
	| exp DIV exp { $$ = $1 / $3; }
;

term: NUMBER  { $$ = $1 ;}
;

%%
int main(int argc, char **argv)
{
        # if YYDEBUG == 1
        extern int yydebug;
        yydebug = 1;
        #endif
	yyparse();
        return 0;
}

void yyerror(const char* s) {
	fprintf(stderr, "Parse error: %s\n", s);
	exit(1);
}

