%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "utils.c"
#include "lexico.c"
#include "gerar.c"

int yylex();
void yyerror(char *);

/* minhas variaveis */
int i, j;
int tipo_variavel = 0; //0 para inteiro e 1 para logico
char *n1, *n2, expr[100], temp[100], *aux01, *aux02, *aux03 = "";
char auxID[100], auxID2[100];
long int tam, pos;
%}

// Simbolo de partida
%start programa

// Simbolos terminais
%token T_PROGRAMA
%token T_INICIO
%token T_FIM
%token T_IDENTIF
%token T_LEIA
%token T_ESCREVA
%token T_ENQTO
%token T_FACA
%token T_FIMENQTO
%token T_PARA
%token T_ATE
%token T_INCR
%token T_FIMPARA
%token T_SE
%token T_ENTAO
%token T_SENAO
%token T_FIMSE
%token T_ATRIB
%token T_VEZES
%token T_DIV
%token T_MAIS
%token T_MENOS
%token T_MAIOR
%token T_MENOR
%token T_IGUAL
%token T_E
%token T_OU
%token T_V
%token T_F
%token T_NUMERO
%token T_NAO
%token T_ABRE
%token T_FECHA
%token T_ABREC
%token T_FECHAC
%token T_LOGICO
%token T_INTEIRO
%token T_VETOR

// Precedencia e associatividade
%left T_E T_OU
%left T_IGUAL
%left T_MAIOR T_MENOR
%left T_MAIS T_MENOS
%left T_VEZES T_DIV

%%

programa: 
	cabecalho {emit(cabecalho); }  
	variaveis
	T_INICIO lista_comandos 
	T_FIM {emit(fechar_contexto); }
	;

cabecalho:
	T_PROGRAMA T_IDENTIF 
	;

variaveis: 
	|declaracao_variaveis 
	;

declaracao_variaveis:
	declaracao_variaveis   
	tipo {CONTA_VARS=0;}
	lista_variaveis
	{ 
		if(tipo_variavel == 1)
		{ 
			for(i=0; i<CONTA_VARS; i++)
			{
				emit(declarar_boolean, TSIMB[j].id, TSIMB[j].tamanho);
				j++;
			}
		}
		else
		{
			for(i=0; i<CONTA_VARS; i++)
			{
				emit(declarar_inteiro, TSIMB[j].id, TSIMB[j].tamanho);
				j++;
			}
		}
	}
	|tipo {CONTA_VARS=0;}
	lista_variaveis 
	{ 
		if(tipo_variavel == 1) 
		{
			for(i=0; i<CONTA_VARS; i++)
			{
				emit(declarar_boolean, TSIMB[j].id, TSIMB[j].tamanho);
				j++;
			}
		}
		else
		{
			for(i=0; i<CONTA_VARS; i++)
			{
				emit(declarar_inteiro, TSIMB[j].id, TSIMB[j].tamanho);
				j++;
			}
		}
	}        
;

tipo:
	T_LOGICO {tipo_variavel=1;}
	|T_INTEIRO {tipo_variavel=0;}
	;

lista_variaveis:
	lista_variaveis T_IDENTIF { insere_variavel (atomo,tipo_variavel, 0); CONTA_VARS++; } 
	|T_IDENTIF { insere_variavel (atomo,tipo_variavel, 0); CONTA_VARS++; }
	|lista_variaveis T_VETOR { identificaVetor(atomo, auxID, &tam); insere_variavel(auxID, tipo_variavel, tam); CONTA_VARS++; } 
	|T_VETOR { identificaVetor(atomo, auxID, &tam); insere_variavel(auxID, tipo_variavel, tam); CONTA_VARS++; } 
	;

lista_comandos:
	| comando lista_comandos
	;

comando:
	entrada_saida
	| repeticao
	| repeticaoPara
	| selecao
	| atribuicao
	;

entrada_saida:
	leitura
	| escrita
	;

leitura:
		T_LEIA  
		T_IDENTIF 
		{ 
			POS_SIMB = busca_simbolo (atomo);
			if (POS_SIMB == -1)
				ERRO ("Variavel [%s] nao declarada!", atomo);
			else
			{
				emit(leitura,atomo,TSIMB[POS_SIMB].tipo, 0); 
			}
		}
		|T_LEIA
		T_VETOR
		{
			identificaVetor(atomo, auxID, &tam);
			POS_SIMB = busca_simbolo(auxID);
			if (POS_SIMB == -1)
				ERRO("Variavel [%s] nao declarada!", auxID);
			else
			{
				identificaVetor(atomo, auxID, &pos);
				emit(leitura, atomo,TSIMB[POS_SIMB].tipo, pos); 
			}
		}
	;

escrita:
	T_ESCREVA expressao
	{ 
		aux01 = desempilhaAux();
	  	emit(imprime, aux01, tipo_variavel, "0");
	  	strcpy(aux01, "");
	}
	|T_ESCREVA expressao T_ABREC expressao
	{
		aux01 = desempilhaAux();
		aux02 = desempilhaAux();
		emit(imprime, aux02, tipo_variavel, aux01);
		strcpy(aux01, "");
		strcpy(aux02, "");
	}
	T_FECHAC
	|T_ESCREVA T_VETOR
	{
		identificaVetor(atomo, auxID, &tam);
		sprintf(auxID2, "%ld", tam);
		emit(imprime, auxID, tipo_variavel, auxID2);
	}
	;

repeticao:
	T_ENQTO
	expressao
	{ 
		aux01 = desempilhaAux();
		emit(enquanto, aux01);
		strcpy(aux01, "");
		identa++;
	}
	T_FACA 
	lista_comandos 
	T_FIMENQTO
	{
		identa--;
		emit(fimenquanto);         
	}
	;

repeticaoPara:
	T_PARA
	{
		emit(para);
	}
	atribuicao
	T_ATE
	termo
	{
		aux01 = desempilhaAux();
		emit(ate, aux01);
		strcpy(aux01, "");
	}
	T_INCR
	termo
	{
		aux01 = desempilhaAux();
		emit(incremento, aux01);
		strcpy(aux01, "");
		identa++;
	}
	T_FACA
	lista_comandos
	T_FIMPARA
	{
		identa--;
		emit(fimpara);
	}
	;

selecao:
	T_SE 
	expressao 
	{
		aux01 = desempilhaAux();
		emit(se, aux01);
		strcpy(aux01, "");  
		identa++;      
	}  
	T_ENTAO
	lista_comandos 
	T_SENAO 
	{
		identa--;
		emit(senao);
		identa++;
	}
	lista_comandos
	T_FIMSE
	{ 
		identa--;  
		emit(fimse);  
	}
	;

atribuicao:	
	T_IDENTIF 
	{ 
		POS_SIMB = busca_simbolo(atomo);
		if (POS_SIMB == -1)
			ERRO ("Variavel [%s] nao declarada!", atomo);
		else
			empilhaAux(TSIMB[POS_SIMB].id);
	}
	T_ATRIB 
	expressao
	{ 
		emit(atribuicao, desempilhaAux(), desempilhaAux(), "0");
	}
	|T_IDENTIF
	{
		POS_SIMB = busca_simbolo(atomo);
		if (POS_SIMB == -1)
			ERRO ("Variavel [%s] nao declarada!", auxID);
		else
			empilhaAux(TSIMB[POS_SIMB].id);
	}
	T_ABREC
	expressao
	T_FECHAC
	T_ATRIB
	expressao
	{
		emit(atribuicao, desempilhaAux(), desempilhaAux(), desempilhaAux());
	}
	|T_VETOR
	{
		identificaVetor(atomo, auxID, &tam);
		POS_SIMB = busca_simbolo(auxID);
		if (POS_SIMB == -1)
			ERRO ("Variavel [%s] nao declarada!", auxID);
		else
			empilhaAux(TSIMB[POS_SIMB].id);
	}
	T_ATRIB
	expressao
	{
		sprintf(auxID2, "%ld", tam);
		emit(atribuicao, desempilhaAux(), auxID2, desempilhaAux());
	}
	;

expressao:
	expressao T_VEZES expressao 
	{ 
		n2 = desempilhaAux();
	  	n1 = desempilhaAux();

		strcat(expr, n1);
		strcat(expr, " * ");
		strcat(expr, n2);
		
		empilhaAux(expr);
		strcpy(expr, ""); 
	}
	| expressao T_DIV expressao
		{ 
			n2 = desempilhaAux();
			n1 = desempilhaAux();

			strcat(expr, n1);
			strcat(expr, " / ");
			strcat(expr, n2);
		
			empilhaAux(expr);
			strcpy(expr, "");
		}
	| expressao T_MAIS expressao
	{ 
		n2 = desempilhaAux();
	  n1 = desempilhaAux();

	  strcat(expr, n1);
	  strcat(expr, " + ");
	  strcat(expr, n2);
	
	  empilhaAux(expr);
	  strcpy(expr, "");
	}
	| expressao T_MENOS expressao
	{ 
	  n2 = desempilhaAux();
	  n1 = desempilhaAux();

	  strcat(expr, n1);
	  strcat(expr, " - ");
	  strcat(expr, n2);
	
	  empilhaAux(expr);
	  strcpy(expr, "");
	}
	| expressao T_MAIOR expressao
	{ 
	  n2 = desempilhaAux();
	  n1 = desempilhaAux();

	  strcat(expr, n1);
	  strcat(expr, " > ");
	  strcat(expr, n2);
	
	  empilhaAux(expr);
	  strcpy(expr, "");
	}
	| expressao T_MENOR expressao
	{ 
	  n2 = desempilhaAux();
	  n1 = desempilhaAux();

	  strcat(expr, n1);
	  strcat(expr, " < ");
	  strcat(expr, n2);
	
	  empilhaAux(expr);
	  strcpy(expr, "");
	}
	| expressao T_IGUAL expressao
	{ 
	  n2 = desempilhaAux();
	  n1 = desempilhaAux();

	  strcat(expr, n1);
	  strcat(expr, " == ");
	  strcat(expr, n2);
	
	  empilhaAux(expr);
	  strcpy(expr, "");
	}
	| expressao T_E expressao
	{ 
	  n2 = desempilhaAux();
	  n1 = desempilhaAux();

	  strcat(expr, n1);
	  strcat(expr, " && ");
	  strcat(expr, n2);
	
	  empilhaAux(expr);
	  strcpy(expr, "");
	}
	| expressao T_OU expressao
	{ 
	  n2 = desempilhaAux();
	  n1 = desempilhaAux();

	  strcat(expr, n1);
	  strcat(expr, " || ");
	  strcat(expr, n2);
	
	  empilhaAux(expr);
	  strcpy(expr, "");
	}
	| termo
	;

termo:
	T_IDENTIF
	{
		POS_SIMB = busca_simbolo(atomo);
		if (POS_SIMB == -1)
			ERRO ("Variavel [%s] nao declarada!", atomo);
		else
		{
			tipo_variavel = TSIMB[POS_SIMB].tipo;
			empilhaAux(TSIMB[POS_SIMB].id);
		}   
	}
	| T_NUMERO
	{ 
		empilhaAux(atomo);
	} 
	| T_V
	{
		strcpy(temp, "true");
		empilhaAux(temp);
	} 
	| T_F
	{ 
		strcpy(temp, "false");
		empilhaAux(temp);
	} 
	| T_NAO termo
	{
		strcpy(temp, "!");
		empilhaAux(strcat(temp, desempilhaAux()));
	}
	| T_ABRE expressao T_FECHA
	{
		strcpy(temp, "(");
		empilhaAux(strcat(strcat(temp, desempilhaAux()), ")"));
	}
	;

%%
/*+--------------------------------------------------------+
  |          Corpo principal do programa COMPILADOR        |
  +--------------------------------------------------------+*/

int yywrap () 
{
	return 1;
}

void yyerror (char *s)
{
  ERRO ("ERRO SINTATICO");
}

int main ()
{
  yyparse ();
}
