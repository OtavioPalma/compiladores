%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include "utils.c"
#include "lexico.c"
#include "gerar.c"

int yylex();
void yyerror(char *);

/* minhas variaveis */
int i, j, k, tipo1, tipo2;
int tipo_variavel = 0; //0 para inteiro e 1 para logico
char *n1, *n2, expr[100], temp[100], *aux01, *aux02, *aux03 = "";
char auxID[100], auxID2[100];
long int tam, pos;
%}

// Simbolo de partida
%start programa

// Simbolos terminais
%token T_PROGRAMA
%token T_INICIO
%token T_FIM
%token T_IDENTIF
%token T_LEIA
%token T_ESCREVA
%token T_ENQTO
%token T_FACA
%token T_FIMENQTO
%token T_PARA
%token T_ATE
%token T_INCR
%token T_FIMPARA
%token T_SE
%token T_ENTAO
%token T_SENAO
%token T_FIMSE
%token T_ATRIB
%token T_VEZES
%token T_DIV
%token T_MAIS
%token T_MENOS
%token T_MAIOR
%token T_MENOR
%token T_IGUAL
%token T_E
%token T_OU
%token T_V
%token T_F
%token T_NUMERO
%token T_NAO
%token T_ABRE
%token T_FECHA
%token T_ABREC
%token T_FECHAC
%token T_LOGICO
%token T_INTEIRO
%token T_VETOR
%token T_FUNCAO
%token T_FIMFUNC
%token T_RETORNA

// Precedencia e associatividade
%left T_E T_OU
%left T_IGUAL
%left T_MAIOR T_MENOR
%left T_MAIS T_MENOS
%left T_VEZES T_DIV

%%

programa: 
	cabecalho {emit(cabecalho); }  
	funcao
	variaveis
	T_INICIO {emit(imprime_main);} 
	lista_comandos 
	T_FIM {emit(fechar_contexto); }
	|cabecalho {emit(cabecalho); }  
	variaveis
	T_INICIO {emit(imprime_main);} 
	lista_comandos 
	T_FIM {emit(fechar_contexto); }
	;

funcao:
	T_FUNCAO
	tipo
	T_IDENTIF
	{
		insere_variavel(atomo, tipo_variavel, 0); 
		CONTA_VARS++;
		emit(funcao, TSIMB[j].id, TSIMB[j].tipo);
		j++;
	}
	T_ABRE
	tipo {CONTA_VARS = 0;}
	lista_variaveis
	{
		tipo2 = tipo_variavel;
		k = CONTA_VARS;
		if(k == 0)
		{
			emit(imprime_funcao);
		}
		else
		{
			for(i = 0; i < CONTA_VARS; i++)
			{
				emit(declarar_parametro, TSIMB[j++].id, tipo_variavel, k);
			}
		}
	}
	T_FECHA {identa++;} T_ENTAO	
	variaveis 
	lista_comandos
	T_RETORNA expressao
	{
		aux01 = desempilhaAux();
		tipo1 = desempilha();

		if(tipo1 == tipo2)
		{
			emit(retorna, aux01);
			identa--; 
			emit(fimenquanto);
		}
		else
		{
			ERRO("Retorno incompatível!");
		}
	}
	T_FIMFUNC
	;

cabecalho:
	T_PROGRAMA T_IDENTIF 
	;

variaveis: 
	|declaracao_variaveis 
	;

declaracao_variaveis:
	declaracao_variaveis   
	tipo {CONTA_VARS=0;}
	lista_variaveis
	{ 
		if(tipo_variavel == 1)
		{ 
			for(i=0; i<CONTA_VARS; i++)
			{
				emit(declarar_boolean, TSIMB[j].id, TSIMB[j].tamanho);
				j++;
			}
		}
		else
		{
			for(i=0; i<CONTA_VARS; i++)
			{
				emit(declarar_inteiro, TSIMB[j].id, TSIMB[j].tamanho);
				j++;
			}
		}
	}
	|tipo {CONTA_VARS=0;}
	lista_variaveis 
	{ 
		if(tipo_variavel == 1) 
		{
			for(i=0; i<CONTA_VARS; i++)
			{
				emit(declarar_boolean, TSIMB[j].id, TSIMB[j].tamanho);
				j++;
			}
		}
		else
		{
			for(i=0; i<CONTA_VARS; i++)
			{
				emit(declarar_inteiro, TSIMB[j].id, TSIMB[j].tamanho);
				j++;
			}
		}
	}        
;

tipo:
	|T_LOGICO {tipo_variavel = 1;}
	|T_INTEIRO {tipo_variavel = 0;}
	;

lista_variaveis:
	|lista_variaveis T_IDENTIF { insere_variavel (atomo,tipo_variavel, 0); CONTA_VARS++; } 
	|T_IDENTIF { insere_variavel (atomo,tipo_variavel, 0); CONTA_VARS++; }
	|lista_variaveis T_VETOR { identificaVetor(atomo, auxID, &tam); insere_variavel(auxID, tipo_variavel, tam); CONTA_VARS++; strcpy(auxID, ""); } 
	|T_VETOR { identificaVetor(atomo, auxID, &tam); insere_variavel(auxID, tipo_variavel, tam); CONTA_VARS++; strcpy(auxID, "");} 
	;

lista_comandos:
	| comando lista_comandos
	;

comando:
	entrada_saida
	| repeticao
	| repeticaoPara
	| selecao
	| atribuicao
	;

entrada_saida:
	leitura
	| escrita
	;

leitura:
		T_LEIA  
		T_IDENTIF 
		{ 
			POS_SIMB = busca_simbolo (atomo);
			if (POS_SIMB == -1)
				ERRO ("Variavel [%s] nao declarada!", atomo);
			else
			{
				emit(leitura,atomo,TSIMB[POS_SIMB].tipo, 0); 
			}
		}
		|T_LEIA
		T_VETOR
		{
			identificaVetor(atomo, auxID, &tam);
			POS_SIMB = busca_simbolo(auxID);
			if (POS_SIMB == -1)
				ERRO("Variavel [%s] nao declarada!", auxID);
			else
			{
				identificaVetor(atomo, auxID, &pos);
				emit(leitura, atomo,TSIMB[POS_SIMB].tipo, pos); 
			}
		}
	;

escrita:
	T_ESCREVA expressao
	{ 
		aux01 = desempilhaAux();
	  	emit(imprime, aux01, tipo_variavel, "0");
	  	strcpy(aux01, "");
	}
	|T_ESCREVA expressao T_ABREC expressao
	{
		aux01 = desempilhaAux();
		aux02 = desempilhaAux();
		emit(imprime, aux02, tipo_variavel, aux01);
		strcpy(aux01, "");
		strcpy(aux02, "");
	}
	T_FECHAC
	|T_ESCREVA T_VETOR
	{
		identificaVetor(atomo, auxID, &tam);
		sprintf(auxID2, "%ld", tam);
		emit(imprime, auxID, tipo_variavel, auxID2);
	}
	;

repeticao:
	T_ENQTO
	expressao
	{ 
		aux01 = desempilhaAux();
		emit(enquanto, aux01);
		strcpy(aux01, "");
		identa++;
	}
	T_FACA 
	lista_comandos 
	T_FIMENQTO
	{
		identa--;
		emit(fimenquanto);         
	}
	;

repeticaoPara:
	T_PARA
	{
		emit(para);
	}
	atribuicao
	T_ATE
	termo
	{
		aux01 = desempilhaAux();
		emit(ate, aux01);
		strcpy(aux01, "");
	}
	T_INCR
	termo
	{
		aux01 = desempilhaAux();
		emit(incremento, aux01);
		strcpy(aux01, "");
		identa++;
	}
	T_FACA
	lista_comandos
	T_FIMPARA
	{
		identa--;
		emit(fimpara);
	}
	;

selecao:
	T_SE 
	expressao 
	{
		aux01 = desempilhaAux();
		emit(se, aux01);
		strcpy(aux01, "");  
		identa++;      
	}  
	T_ENTAO
	lista_comandos 
	T_SENAO 
	{
		identa--;
		emit(senao);
		identa++;
	}
	lista_comandos
	T_FIMSE
	{ 
		identa--;  
		emit(fimse);  
	}
	;

atribuicao:	
	T_IDENTIF 
	{ 
		POS_SIMB = busca_simbolo(atomo);
		if (POS_SIMB == -1)
			ERRO ("Variavel [%s] nao declarada!", atomo);
		else
		{
			empilhaAux(TSIMB[POS_SIMB].id);
			empilha(TSIMB[POS_SIMB].tipo);
		}
	}
	T_ATRIB 
	expressao
	{ 
		tipo1 = desempilha();
		tipo2 = desempilha();

		if(tipo1 == tipo2)
			emit(atribuicao, desempilhaAux(), desempilhaAux(), "0");
		else
			ERRO ("Tipos Incompatíveis");
	}
	|T_IDENTIF
	{
		POS_SIMB = busca_simbolo(atomo);
		if (POS_SIMB == -1)
			ERRO ("Variavel [%s] nao declarada!", auxID);
		else
		{
			empilhaAux(TSIMB[POS_SIMB].id);
			empilha(TSIMB[POS_SIMB].tipo);
		}
	}
	T_ABREC
	expressao
	T_FECHAC
	T_ATRIB
	expressao
	{
		tipo1 = desempilha();
		tipo2 = desempilha();

		if(tipo1 == tipo2)
			emit(atribuicao, desempilhaAux(), desempilhaAux(), desempilhaAux());
		else
			ERRO ("Tipos Incompatíveis");
	}
	|T_VETOR
	{
		identificaVetor(atomo, auxID, &tam);
		POS_SIMB = busca_simbolo(auxID);
		if (POS_SIMB == -1)
			ERRO ("Variavel [%s] nao declarada!", auxID);
		else
		{
			empilhaAux(TSIMB[POS_SIMB].id);
			empilha(TSIMB[POS_SIMB].tipo);
		}
	}
	T_ATRIB
	expressao
	{
		tipo1 = desempilha();
		tipo2 = desempilha();

		if(tipo1 == tipo2)
		{
			sprintf(auxID2, "%ld", tam);
			emit(atribuicao, desempilhaAux(), auxID2, desempilhaAux());
		}
		else
			ERRO ("Tipos Incompatíveis");
	}
	;

expressao:
	expressao T_VEZES expressao 
	{ 
		tipo1 = desempilha();
		tipo2 = desempilha();

		if(tipo1 == tipo2)
		{
			n2 = desempilhaAux();
			n1 = desempilhaAux();

			strcat(expr, n1);
			strcat(expr, " * ");
			strcat(expr, n2);
			
			empilhaAux(expr);
			strcpy(expr, ""); 
			empilha(tipo1);
		}
		else
		{
			ERRO ("Tipos Incompatíveis");
		}
	}
	| expressao T_DIV expressao
	{ 
		tipo1 = desempilha();
		tipo2 = desempilha();

		if(tipo1 == tipo2)
		{
			n2 = desempilhaAux();
			n1 = desempilhaAux();

			strcat(expr, n1);
			strcat(expr, " / ");
			strcat(expr, n2);
		
			empilhaAux(expr);
			strcpy(expr, "");
			empilha(tipo1);
		}
		else
		{
			ERRO ("Tipos Incompatíveis");
		}
	}
	| expressao T_MAIS expressao
	{ 
		tipo1 = desempilha();
		tipo2 = desempilha();

		if(tipo1 == tipo2)
		{
			n2 = desempilhaAux();
			n1 = desempilhaAux();

			strcat(expr, n1);
			strcat(expr, " + ");
			strcat(expr, n2);
			
			empilhaAux(expr);
			strcpy(expr, "");
			empilha(tipo1);
		}
		else
		{
			ERRO ("Tipos Incompatíveis");
		}
	}
	| expressao T_MENOS expressao
	{ 
		tipo1 = desempilha();
		tipo2 = desempilha();

		if(tipo1 == tipo2)
		{
			n2 = desempilhaAux();
			n1 = desempilhaAux();

			strcat(expr, n1);
			strcat(expr, " - ");
			strcat(expr, n2);
			
			empilhaAux(expr);
			strcpy(expr, "");
			empilha(tipo1);
		}
		else
		{
			ERRO ("Tipos Incompatíveis");
		}
	}
	| expressao T_MAIOR expressao
	{ 
		n2 = desempilhaAux();
		n1 = desempilhaAux();

		strcat(expr, n1);
		strcat(expr, " > ");
		strcat(expr, n2);
		
		empilhaAux(expr);
		strcpy(expr, "");
	}
	| expressao T_MENOR expressao
	{ 
		n2 = desempilhaAux();
		n1 = desempilhaAux();

		strcat(expr, n1);
		strcat(expr, " < ");
		strcat(expr, n2);
		
		empilhaAux(expr);
		strcpy(expr, "");
	}
	| expressao T_IGUAL expressao
	{ 
		n2 = desempilhaAux();
		n1 = desempilhaAux();

		strcat(expr, n1);
		strcat(expr, " == ");
		strcat(expr, n2);
		
		empilhaAux(expr);
		strcpy(expr, "");
	}
	| expressao T_E expressao
	{ 
		n2 = desempilhaAux();
		n1 = desempilhaAux();

		strcat(expr, n1);
		strcat(expr, " && ");
		strcat(expr, n2);
		
		empilhaAux(expr);
		strcpy(expr, "");
	}
	| expressao T_OU expressao
	{ 
		n2 = desempilhaAux();
		n1 = desempilhaAux();

		strcat(expr, n1);
		strcat(expr, " || ");
		strcat(expr, n2);
		
		empilhaAux(expr);
		strcpy(expr, "");
	}
	| termo
	;

termo:
	T_IDENTIF
	{
		POS_SIMB = busca_simbolo(atomo);
		if (POS_SIMB == -1)
			ERRO ("Variavel [%s] nao declarada!", atomo);
		else
		{
			tipo_variavel = TSIMB[POS_SIMB].tipo;
			empilhaAux(TSIMB[POS_SIMB].id);
			empilha(TSIMB[POS_SIMB].tipo);
		}   
	}
	| T_NUMERO
	{ 
		empilhaAux(atomo);
		empilha(0);
	} 
	| T_V
	{
		strcpy(temp, "true");
		empilhaAux(temp);
		empilha(1);
	} 
	| T_F
	{ 
		strcpy(temp, "false");
		empilhaAux(temp);
		empilha(1);
	} 
	| T_NAO termo
	{
		strcpy(temp, "!");
		empilhaAux(strcat(temp, desempilhaAux()));
	}
	| T_ABRE expressao T_FECHA
	{
		strcpy(temp, "(");
		empilhaAux(strcat(strcat(temp, desempilhaAux()), ")"));
	}
	;

%%
/*+--------------------------------------------------------+
  |          Corpo principal do programa COMPILADOR        |
  +--------------------------------------------------------+*/

int yywrap () 
{
	return 1;
}

void yyerror (char *s)
{
  ERRO ("ERRO SINTATICO");
}

int main ()
{
  yyparse ();
}
