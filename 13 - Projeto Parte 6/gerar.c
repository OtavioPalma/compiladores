/***********************************************
* Função responsável por gerar código em C
* Paulo Muniz de Ávila. 
************************************************/


enum instrucao {cabecalho = 1,declarar_inteiro = 2, declarar_boolean, fechar_contexto, leitura, imprime, se,
senao, fimse, enquanto, fimenquanto, para, ate, incremento, fimpara, atribuicao, funcao, declarar_parametro,
retorna, imprime_main, fechaFunc, imprime_funcao};
int posAtual = 1;
int controle = 0;

void emitTab()
{
	int i;
	for(i = 0; i < identa; i++)
		printf("   ");
}

void emit(int c,...)
{
	char *id, *tamVetor, *aux, *aux2;
	int tipo, num1, num2, pos;
	va_list argp;

	switch(c)
	{
		case cabecalho:
			printf("#include <stdio.h>\n");
			printf("#include <stdlib.h>\n");
			printf("#include <math.h>\n");
			printf("#include <stdbool.h>\n\n");
		break;

		case declarar_inteiro:
			emitTab();
			va_start(argp, c);
			id = va_arg(argp, char*);
			num1 = va_arg(argp, int);

			if(num1 == 0)
				printf("int %s;\n", id);
			else
				printf("int %s[%d];\n", id, num1);
			va_end(argp);
		break;

		case declarar_boolean:
			emitTab();
			va_start (argp, c);
			id = va_arg(argp, char *); 
			num1 = va_arg(argp, int);

			if(num1 == 0)	
				printf("boolean %s;\n", id);
			else
				printf("boolean %s[%d];\n", id, num1);
			va_end(argp);
		break;

	  	case fechar_contexto:
			printf("\nreturn 0; \n}\n");
 			break;

		case leitura:
			emitTab();
			printf("scanf(");
			va_start (argp, c);
			id = va_arg(argp, char *); 
			printf("\"%cd\",&%s", '%',id);
			printf(");\n");
			va_end(argp);
		break;

 	  	case imprime:
			emitTab();
			printf("printf(");
			va_start (argp, c);
			id = va_arg(argp, char *); 
			tipo = va_arg(argp, int);
			aux = va_arg(argp, char*);

			if(strcmp(aux, "0") == 0)
			{
				if(tipo == 1)
					printf("%s ? \"true\" : \"false\"", id);
				else
					printf("\"%cd\",%s", '%',id);
				printf(");\n");
			}
			else
			{
				if(tipo == 1)
					printf("%s ? \"true\" : \"false\"", id);
				else
					printf("\"%cd\",%s[%s]", '%', id, aux);
				printf(");\n");
			}
			va_end(argp);
		break;

	  	case se: 
			emitTab();
			printf("if(");
			va_start (argp, c);
			id = va_arg(argp, char*);
			printf("%s", id);
			printf(")\n");
			emitTab();
			printf("{\n");
			va_end(argp);
		break;

	  	case senao:
			emitTab();
			printf("}\n");
			emitTab();
			printf("else\n");
			emitTab();
			printf("{\n");
		break;

	  	case fimse:
			emitTab();
			printf("}\n");
		break;

	  	case enquanto:
			emitTab();
			printf("while(");
			va_start (argp, c);
			id = va_arg(argp, char*);
			printf("%s", id);
			printf(")\n");
			emitTab();
			printf("{\n");
			va_end(argp);	
		break;

	  	case fimenquanto:
			emitTab();
			printf("}\n\n");	
		break;

	  	case para:
	  		emitTab();
			printf("for(");
			controle = 1;
		break;

	  	case ate:
			va_start(argp, c);
			id = va_arg(argp, char*);
			num2 = atoi(id);
			if(num1 > num2)
				printf(" i > %s;", id); 
			else
				printf(" i < %s;", id);
			va_end(argp);
		break;

	  	case incremento:
			va_start(argp, c);
			id = va_arg(argp, char*);
			num2 = atoi(id);
			if(num2 == 1)
			{
				printf(" i++)\n");
				emitTab();
				printf("{\n");
			}
			else
			{
				printf(" i = i + %s)\n", id);
				emitTab();
				printf("{\n");
			}
		break;		
		
	  	case fimpara:
			emitTab();
			printf("}\n");
		break;
	  
	  	case atribuicao:
			if(controle == 0)
			{
				emitTab();
				va_start(argp, c);
				id = va_arg(argp, char*);
				aux = va_arg(argp, char*);
				aux2 = va_arg(argp, char*);
				if(strcmp(aux2, "0") == 0)
					printf("%s = %s;\n", id, aux);
				else
					printf("%s[%s] = %s;\n", id, aux, aux2);
				va_end(argp);
				break;
			}
			else
			{
				controle = 0;
				va_start(argp, c);
				id = va_arg(argp, char*);
				aux = va_arg(argp, char*);
				printf("%s = %s;", id, aux);
				va_end(argp);
				break;
			}

		case funcao:
			emitTab();
			va_start(argp, c);
			id = va_arg(argp, char*);
			tipo = va_arg(argp, int);
			if(tipo == 0)
				aux = "int";
			else 
				aux = "boolean";
			printf("%s %s(", aux, id);
			va_end(argp);
			break;

		case declarar_parametro:
			va_start(argp, c);
			id = va_arg(argp, char*);
			tipo = va_arg(argp, int);
			pos = va_arg(argp, int);

			if(posAtual == pos)
			{
				if(tipo == 0)
				{
					printf("%s %s)\n", "int", id);
					emitTab();
					printf("{\n");
				}
				else
				{
					printf("%s %s)\n", "boolean", id);
					emitTab();
					printf("{\n");
				}
				posAtual = 1;
			}
			else
			{
				if(tipo == 0)
					printf("%s %s, ", "int", id);
				else
					printf("%s %s, ", "boolean", id);
				posAtual++;
			}
			break;

		case retorna:
			emitTab();
			va_start(argp, c);
			id = va_arg(argp, char*);
			printf("return %s;\n", id);
			break;

		case imprime_main:
			printf("\nvoid main()\n");
			printf("{\n");
			break;

		case fechaFunc:
			printf(")\n");
			emitTab();
			printf("{");
			break;

		case imprime_funcao:
			printf(")\n");
			emitTab();
			printf("{\n");
   }
}