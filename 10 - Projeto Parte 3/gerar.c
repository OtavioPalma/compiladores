/***********************************************
* Função responsável por gerar código em C
* Paulo Muniz de Ávila. 
************************************************/


enum instrucao {cabecalho=1,declarar_inteiro=2,declarar_boolean=3,fechar_contexto=4,leitura=5,imprime=6,se=7,
senao=8,fimse=9, enquanto=10, fimenquanto=11, para=12, ate=13, incremento=14, fimpara=15, atribuicao=16, atribuicaoPara=17};

int controle = 0;

void emitTab() {
   int i;
   for(i = 0; i < identa; i++)
      printf("   ");
}

void emit(int c,...) {
    char *id, *aux;
    int tipo, num1, num2;
     va_list argp;

   switch(c) {
	case cabecalho:
		printf("#include <stdio.h>\n");
		printf("#include <stdlib.h>\n");
		printf("#include <math.h>\n");
		printf("#include <stdbool.h>\n");
		printf("\nint main() {\n");
		break;

        case declarar_inteiro:
		emitTab();
		printf("int ");
   	        va_start (argp, c);
  	        id = va_arg(argp, char *); 
	        printf("%s", id);
                printf(";\n");
                va_end(argp);
		break;

        case declarar_boolean:
		emitTab();
		printf("bool ");
                va_start (argp, c);
  	        id = va_arg(argp, char *); 
	        printf("%s", id);
                printf(";\n");
                va_end(argp);
		break;

	  case fechar_contexto:
		printf("\nreturn 0; \n}\n");
 		break;

          case leitura:
		emitTab();
		printf("scanf(");
   	        va_start (argp, c);
  	        id = va_arg(argp, char *); 
                printf("\"%cd\",&%s", '%',id);
                printf(");\n");
                va_end(argp);
		break;

 	  case imprime:
		emitTab();
		printf("printf(");
   	        va_start (argp, c);
  	        id = va_arg(argp, char *); 
                tipo = va_arg(argp, int);
                if(tipo == 1) {
                  printf("%s ? \"true\" : \"false\"", id);
                } else {
                   printf("\"%cd\",%s", '%',id);
                }
                printf(");\n");
                va_end(argp);
		break;

	  case se: 
		emitTab();
		printf("if(");
		va_start (argp, c);
		id = va_arg(argp, char*);
		printf("%s", id);
		printf(") {\n");
		va_end(argp);
		break;

	  case senao:
		emitTab();
		printf("} else {\n");
		break;

	  case fimse:
		emitTab();
		printf("}\n");
		break;

	  case enquanto:
		emitTab();
		printf("while(");
		va_start (argp, c);
		id = va_arg(argp, char*);
		printf("%s", id);
		printf(") {\n");
		va_end(argp);
		break;

	  case fimenquanto:
		emitTab();
		printf("}\n");
		break;

	  case para:
	  	emitTab();
		printf("for(");
		controle = 1;
		break;

	  case ate:
		va_start(argp, c);
		id = va_arg(argp, char*);
		num2 = atoi(id);

		if(num1 > num2)
		{
		   printf(" i > %s;", id); 
		}
		else
		{
		   printf(" i < %s;", id);
		}
		va_end(argp);
		break;

	  case incremento:
		va_start(argp, c);
		id = va_arg(argp, char*);
		num2 = atoi(id);
		
		if(num2 == 1)
		{
		   printf(" i++){\n");
		}
		else
		{
		   printf(" i = i + %s){\n", id);
		}

		break;		
		
	  case fimpara:
		emitTab();
		printf("}\n");
		break;
	  
	  case atribuicao:
		if(controle == 0)
		{
			emitTab();
			va_start(argp, c);
			id = va_arg(argp, char*);
			aux = va_arg(argp, char*);
			num1 = atoi(aux);
			printf("%s = %s;\n", id, aux);
			va_end(argp);
			break;
		}
		else
		{
			controle = 0;
			va_start(argp, c);
			id = va_arg(argp, char*);
			aux = va_arg(argp, char*);
			num1 = atoi(aux);
			printf("%s = %s;", id, aux);
			va_end(argp);
			break;
		}
   }
}


