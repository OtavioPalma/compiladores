%{

int token = 1;
int linha = 1;

%}

%%

[0-9A-F]+ 	{ printf("Número %s\n", yytext); token++; }
\n		{ printf("\nNOVA LINHA\n"); linha++; }
[ \t]		{ token++; }
.		{ printf("Erro: %s, no token: %d, na linha: %d\n", yytext, token, linha); }

%%

int main(int argc, char **argv)
{
   yylex();
   return 0;
}
