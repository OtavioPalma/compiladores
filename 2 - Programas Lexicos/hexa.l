/* 
 * Description: Recognize the 32-bit hexadecimal integer from stdin
 * Pattern: 0[xX]([0-9a-fA-F]{1,8})
 */

%{
   #include <stdio.h>
   #include <string.h>
   #include <stdlib.h>

   int i = 0;
   char *hexa; //buffer para armazenar hexadecimal
   int flag = 1;  //flag para verificar se o hexadecimal é válido
%}



digit		[0-9]
alpha		[a-zA-Z]
hextail		({digit}|{alpha}){1,8}
hex		0[x]{hextail}


%%
{hex}	{
             hexa = (char *) malloc(strlen(yytext) * sizeof(char));
             strcpy(hexa,yytext);
             char *temp = hexa; 
             while(*temp != '\0'){	
                if((*temp != 'x') && (*temp < '0' || *temp > 'F'))
                {
                   flag = 0;
                   break;
                }
                temp++; 	
             }
             if(flag) 
             {
                 printf("Hex: %s", hexa);
             } else {
                 printf("NOT Hex: %s", hexa);
                 flag = 1;
             }

             free(hexa);          
  
        }

.	{ printf("%c",' '); }
%%

int main()
{
  yylex();
  return 0;
}
