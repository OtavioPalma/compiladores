//Otávio Messias Palma

%{

int token = 1;
int linha = 1;

%}

%%

"program"    	{ printf("simb_program\n"); token++; }
"var"    	{ printf("simb_var\n"); token++; }
";"    		{ printf("simb_pv\n"); token++; }
":"    		{ printf("simb_dp\n"); token++; }
"begin"    	{ printf("simb_begin\n"); token++; }
"integer"	{ printf("p_res_integer\n"); token++; }
"while"		{ printf("simb_while\n"); token++; }
":=" 		{ printf("simb_atrib\n"); token++; }
"("		{ printf("simb_apar\n"); token++; }
")" 		{ printf("simb_fpar\n"); token++; }
"<"		{ printf("simb_menor\n"); token++; }
"do"		{ printf("simb_do\n"); token++; }
"+"		{ printf("simb_mais\n"); token++; }
"end" 		{ printf("simb_end\n"); token++; }
"."		{ printf("simb_p\n"); token++; }
[0-9]+ 		{ printf("num\n"); token++; }
[a-zA-Z]+	{ printf("id\n"); token++; }
\n		{ printf("\nNOVA LINHA\n"); linha++; }
[ \t]		{ token++; }
.		{ printf("Erro: %s, no token: %d, na linha: %d\n", yytext, token, linha); }

%%

int main(int argc, char **argv)
{
   yylex();
   return 0;
}
